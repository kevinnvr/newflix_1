import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EjemplobootstrapComponent } from './ejemplobootstrap/ejemplobootstrap.component';
import { EjemplomodalComponent } from './ejemplomodal/ejemplomodal.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    EjemplobootstrapComponent,
    EjemplomodalComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
