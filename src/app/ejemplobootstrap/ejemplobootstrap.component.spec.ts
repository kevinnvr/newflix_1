import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EjemplobootstrapComponent } from './ejemplobootstrap.component';

describe('EjemplobootstrapComponent', () => {
  let component: EjemplobootstrapComponent;
  let fixture: ComponentFixture<EjemplobootstrapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EjemplobootstrapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EjemplobootstrapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
