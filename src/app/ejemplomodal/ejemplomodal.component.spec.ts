import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EjemplomodalComponent } from './ejemplomodal.component';

describe('EjemplomodalComponent', () => {
  let component: EjemplomodalComponent;
  let fixture: ComponentFixture<EjemplomodalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EjemplomodalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EjemplomodalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
